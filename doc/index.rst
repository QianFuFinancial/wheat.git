.. wheat documentation master file, created by
   sphinx-quickstart on Fri Nov 10 10:37:31 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to wheat's documentation! |CI status|
================================================

Wheat is a Python library for generating python project.

Installation
------------

Requirements
~~~~~~~~~~~~

-  Python 2.7 and up

``$ pip install wheat``

Usage
-----

.. code:: bash

    wheat

    > Enter your project name:
    > Enter your name:
    > Enter the output dir:

    > 1 - mock-logger: Handler for testing logging[Recommended]
    > 2 - config: Reading configs from yaml files[Recommended]
    > 3 - paddy: Transaction Middlewares
    > 4 - orm: Orm for database connection
    > Choose packages extended:

    > Check for requirements of generated project
    > chance-mock-logger ... [False]

    > Packages to be installed
    > - chance-mock-logger == 1.0.0
    > Proceed? [Y/N]

    > Creating Directories
    > Generating Files
    > Initialization Finished
    > Running checks
    > Successfully create PROJECT at ...

Development
-----------

::

    $ virtualenv wheat
    $ . wheat/bin/activate
    $ pip install -e .


.. toctree::
   :maxdepth: 4
   :caption: Contents:

   wheat


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Contributing
------------

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

License
-------

`MIT`_

.. _MIT: https://choosealicense.com/licenses/mit/

.. |CI status| image:: https://img.shields.io/badge/build-passing-brightgreen.svg
