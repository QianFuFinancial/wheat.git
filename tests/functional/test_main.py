#!/usr/bin/env python
# -*- coding: utf-8 -*-
# File: tests/functional/test_main.py
# Author: Jimin Huang <huangjimin@whu.edu.cn>
# Date: 14.11.2017
import os
import shutil
import sys

from wheat.main import main


class TestMain(object):
    """Test class for ``main.main``"""

    def setUp(self):
        self.orig_stdin = sys.stdin
        sys.stdin = open("tests/fixtures/preprogrammed_inputs.txt")

    def teardown(self):
        sys.stdin = self.orig_stdin
        shutil.rmtree(os.path.join(".", "test"))

    def test_normal_run(self):
        """Check if ``main.main`` works"""
        main()
