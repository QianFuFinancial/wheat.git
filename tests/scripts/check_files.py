#!/usr/bin/env python
# -*- coding: utf-8 -*-
# File: scripts/check_files.py
# Author: MingshiCai i@unoiou.com
# Date: 2020-11-02 15:49:25
import os
import sys

should_not_exist = {
    "api": [],
    "cli": ["ingress.yaml", "test-connection.yaml"],
}

should_exist = {
    "api": ["ingress.yaml", "test-connection.yaml", "deployment.yaml"],
    "cli": [],
}

project_type = sys.argv[-1]

files = []
for _, _, child_files in os.walk("test"):
    files.extend(child_files)

for absent in should_not_exist[project_type]:
    assert absent not in files, f"{absent} should not exist."
for existing in should_exist[project_type]:
    assert existing in files, f"{existing} should exist."
