# -*- coding: utf-8 -*-
# Author: heheqiao <614400597@qq.com>
from nose.tools import assert_equals

from wheat.exceptions import ProjectGenerationError


def test_ProjectGenerationError():
    """Check if ``ProjectGenerationError.__str__`` works
    """
    assert_equals(
        str(ProjectGenerationError("test")), "Generation of test failed"
    )
