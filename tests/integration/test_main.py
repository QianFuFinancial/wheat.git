# -*- coding: utf-8 -*-
# Author: heheqiao <614400597@qq.com>
import os
import shutil

from nose.tools import assert_equals, assert_raises

from wheat import main
from wheat.constant import CONF_RULES
from wheat.exceptions import ProjectGenerationError


def test_create_directories():
    """Check if ``create_directories`` works
    """
    directories = ["test", os.path.join("test", "test1")]
    main.create_directories(directories, os.path.join("."))

    assert os.path.exists(os.path.join(".", "test"))
    assert os.path.exists(os.path.join(".", "test", "test1"))

    shutil.rmtree(os.path.join(".", "test"))


class TestGenerateFiles(object):
    """Test class for ``generate_files``
    """

    output_dir = os.path.join(".", "test")

    def setUp(self):
        os.makedirs(self.output_dir)

    def tearDown(self):
        shutil.rmtree(self.output_dir)

    def test_generate_files_normal(self):
        """Check if ``generate_files`` works with normal module
        """
        main.generate_files(
            ["test.txt"],
            self.output_dir,
            "test",
            "test_repo_url",
            "tester",
            {"test.txt": "test.txt.template"},
            "test",
            "test",
            "test",
            "test",
            "test",
            "",
            " test-api",
        )

        assert os.path.isfile(os.path.join(self.output_dir, "test.txt"))

        with open(os.path.join(self.output_dir, "test.txt"), "r") as f:
            results = f.readlines()
        assert_equals(
            results,
            [
                "test\n",
                "Project: test\n",
                "File: test.txt\n",
                "Author: tester\n",
                "Class: Test\n",
                "Service: test\n",
                "data_mid: test\n",
                "data_import: test\n",
                "services: test\n",
                "requirements: test\n",
                "project_repo_url: test_repo_url\n",
                "extra_dynamic_configs:  test-api\n",
            ],
        )


def test_check_project():
    """Check if project is generated as expectation
    """
    fake_output_dir = "."
    fake_files = [
        os.path.join("wheat", "main.py"),
        os.path.join("setup.cfg"),
    ]

    main.check_project(fake_files, fake_output_dir)


def test_check_project_raise_exception():
    """Check if project is generated as expectation
    """
    fake_output_dir = "."
    fake_files = [
        "test",
        os.path.join("wheat", "main.py"),
        os.path.join("setup.py"),
    ]

    assert_raises(
        ProjectGenerationError, main.check_project, fake_files, fake_output_dir
    )


class TestGenerateDoc(object):
    """Test class for `generate_doc`
    """

    def tearDown(self):
        shutil.rmtree(os.path.join(".", "testsphinx"))

    def test_generate_doc(self):
        """Check if `generate_doc` works
        """
        test_path = os.path.join(".", "testsphinx", "doc")
        os.makedirs(os.path.join(".", "testsphinx", "testsphinx"))
        os.makedirs(test_path)

        main.generate_doc("testsphinx", "tester", ".")

        conf_file = os.path.join(test_path, "conf.py")
        assert os.path.isfile(conf_file)

        with open(conf_file, "r") as conf:
            result = conf.read()
            for value in CONF_RULES.values():
                assert value in result
