#!/usr/bin/env python
# -*- coding: utf-8 -*-
# File              : wheat/main.py
# Author            : Jimin Huang <huangjimin@whu.edu.cn>
# Date              : 13.02.2018
# Last Modified Date: 14.02.2018
# Last Modified By  : Jimin Huang <huangjimin@whu.edu.cn>
# -*- coding: utf-8 -*-
# File: wheat/main.py
# Author: Jimin Huang <huangjimin@whu.edu.cn>
# Date: 14.11.2017
import os
import subprocess

import pkg_resources
from sphinx.ext import apidoc

from wheat import utils
from wheat.constant import AVAILABLE_TYPES, CONF_RULES
from wheat.exceptions import ProjectGenerationError


def main():
    """Read inputs and generate projects"""
    project_name = input("Input your project name: ")
    project_type = input(
        "Input your project type({}): ".format(" | ".join(AVAILABLE_TYPES))
    )
    assert project_type in AVAILABLE_TYPES
    project_repo_url = input("Input your project git repo url: ")
    service_name = input("Input your service name: ")
    author_name = input("Input your author name: ")
    output_dir = input("Input your output dir: ")
    with_data = input("Whether your project will use data middleware [Y/N]:")

    with_general_redis = input(
        "Whether your project need redis connection [Y/N]:"
    )

    dynamic_config_items = []
    indent_item = "    "
    with_data = utils.parse_yes_no(with_data)
    if not with_data:
        data_mid = ""
        data_import = ""
        services = ""
        requirements = ""
    else:
        data_mid = "YamClient.connect(config['data']['host'])\n{}".format(
            indent_item * 1
        )
        data_import = "from yam import YamClient\n"
        services = (
            "services:\n{}- name: mockserver/mockserver:mockserver-5.9.0\n"
        ).format(indent_item)

        dynamic_config_items.extend(
            ['"general-data"', '"{}-data"'.format(project_name)]
        )

        if project_type == "cli":
            requirements = 'httpx = "^0.12.1"\nchance-yam = "^1.0.0"'
        else:
            requirements = 'chance-yam = "^1.0.0"'

    redis_import = ""
    if utils.parse_yes_no(with_general_redis):
        if not with_data:
            services += "services:\n"

        dynamic_config_items.extend(
            ['"general-redis"', '"{}-redis"'.format(project_name)]
        )

        data_mid += """redis_server = redis.StrictRedis(
        host=config['redis']['host'], port=config['redis']['port']
    )
    queue = RpqQueue(redis_server, config['redis']['key'])
    config['redis']['queue'] = queue\n    """

        redis_import = "\nimport redis\nfrom rpq.RpqQueue import RpqQueue\n"
        services += "    - name: redis:latest\n  "
        requirements += '\nredis = "^3.4.1"\nrpq = "^2.2"'

    extra_dynamic_configs = (
        "{}{}".format(
            "\n{}".format(indent_item * 3), ",\n{}".format(indent_item * 3)
        ).join(dynamic_config_items)
        if dynamic_config_items
        else ""
    )

    directories = utils.directories_to_create(project_name)
    files = utils.files_to_generate(project_name, data_mid)
    match_rules = utils.generate_match_rules(files)

    e_files, e_dirs, e_match_rules, e_resource_mgr = get_extra_files_meta(
        project_name, project_type
    )
    directories.extend(e_dirs)
    create_directories(directories, output_dir)

    generate_files(
        files,
        output_dir,
        project_name,
        project_repo_url,
        author_name,
        match_rules,
        service_name,
        data_mid,
        data_import,
        services,
        requirements,
        extra_dynamic_configs=extra_dynamic_configs,
        redis_import=redis_import,
    )
    # 额外的文件中可能有需要覆盖的文件，覆写基本的文件
    generate_files(
        e_files,
        output_dir,
        project_name,
        project_repo_url,
        author_name,
        e_match_rules,
        service_name,
        data_mid,
        data_import,
        services,
        requirements,
        extra_resource_manager=e_resource_mgr,
        extra_dynamic_configs=extra_dynamic_configs,
        redis_import=redis_import,
    )
    generate_doc(project_name, author_name, output_dir)

    files.extend(e_files)
    check_project(files, output_dir)

    subprocess.run(
        ["black", f"{project_name}/{project_name}", f"{project_name}/tests"]
    )

    print(
        "\n".join(
            [
                "✨ Don't forget to add configs of your project to NACOS.",
                "✨ NACOS Groups: staging, production, test",
                "✨ For api projects, add `api.port` and set it to `80`",
            ]
        )
    )


def get_extra_files_meta(project_name, project_type):
    """Generate extra files meta of typical project

    Args:
        project_name: a str as the project name
        project_type: a str as the project type

    Return:
        (files, directories, match_rules, tpl_resource_manager)
    """
    # Overwrite extra files for typical project.
    extra_files_mapping = {
        "cli": [],
        "api": [
            os.path.join(project_name, "pyproject.toml"),
            os.path.join(project_name, "chart", "values.yaml"),
            os.path.join(
                project_name,
                "chart",
                "templates",
                "tests",
                "test-connection.yaml",
            ),
            os.path.join(
                project_name, "chart", "templates", "deployment.yaml"
            ),
            os.path.join(project_name, "chart", "templates", "ingress.yaml"),
            os.path.join(project_name, project_name, "main.py"),
            os.path.join(project_name, project_name, "routers", "__init__.py"),
            os.path.join(project_name, project_name, "routers", "health.py"),
            os.path.join(
                project_name, "tests", "functional", "test_health.py"
            ),
            os.path.join(
                project_name, "tests", "functional", "test_router.py"
            ),
            os.path.join(
                project_name, "tests", "functional", "test_main_functional.py"
            ),
        ],
    }
    extra_dirs_mapping = {
        "cli": [],
        "api": [os.path.join(project_name, project_name, "routers")],
    }
    extra_tpl_resource_manager_mapping = {"cli": "", "api": ".api"}

    files = extra_files_mapping[project_type]
    match_rules = utils.generate_match_rules(files)
    return (
        files,
        extra_dirs_mapping[project_type],
        match_rules,
        extra_tpl_resource_manager_mapping[project_type],
    )


def create_directories(directories, output_dir):
    """Make directories

    Caution:
        Parent directory should in front of the directories,
        and child directory should in the rear.

    Args:
        directories: a list of str
        output_dir: a str as the output directory
    """
    for directory in directories:
        os.makedirs(os.path.join(output_dir, directory))


def generate_files(
    files,
    output_dir,
    project_name,
    project_repo_url,
    author_name,
    match_rules,
    service_name,
    data_mid="",
    data_import="",
    services="",
    requirements="",
    extra_resource_manager="",
    extra_dynamic_configs="",
    redis_import="",
):
    """Generate files with given file path

    Args:
        files: a list of `str`
        output_dir: a `str` as the output directory
        project_name: a `str`
        project_repo_url: a `str`
        author_name: a `str`
        match_rules: a dict as {str: str}
        service_name: a str
        data_mid: a str
        data_import: a str
        services: a str
        requirements: a str
        extra_resource_manager: a str as extra pkg resource manager name
        extra_dynamic_configs: a str
    """
    for f in files:
        template_name = match_rules[f]

        output_file = os.path.join(output_dir, f)

        class_name = "".join(
            [word[0].upper() + word[1:] for word in project_name.split("_")]
        )
        chart_name = "-".join(project_name.split("_"))

        with open(
            pkg_resources.resource_filename(
                "wheat.template" + extra_resource_manager, template_name
            ),
            encoding="utf8",
        ) as template:
            print(f"creating: {template_name}")
            with open(output_file, "w", encoding="utf8") as output:
                replace_rules = {
                    "{PROJECT_NAME}": project_name,
                    "{PROJECT_REPO_URL}": project_repo_url,
                    "{AUTHOR_NAME}": author_name,
                    "{FILE_NAME}": f,
                    "{CLASS_NAME}": class_name,
                    "{ENV_NAME}": project_name.upper(),
                    "{SERVICE_NAME}": service_name,
                    "{CHART_NAME}": chart_name,
                    "{DATA_MID}": data_mid,
                    "{DATA_IMPORT}": data_import,
                    "{SERVICES}": services,
                    "{REQUIREMENTS}": requirements,
                    "{EXTRA_DYNAMIC_CONFIGS}": extra_dynamic_configs,
                    "{REDIS_IMPORT}": redis_import,
                }
                content = template.read()
                for key, value in replace_rules.items():
                    content = content.replace(key, value)
                output.write(content)


def check_project(files, output_dir):
    """Check if project generated

    Args:
        files: a list of str
        output_dir: a str as the output directory

    Raises:
        ProjectGenerationError when files not generated as expected
    """
    for f in files:
        real_path = os.path.join(output_dir, f)
        if os.path.isfile(real_path):
            continue
        raise ProjectGenerationError(f)


def generate_doc(project_name, author_name, output_dir):
    """Generate doc by using `shpinx.apidoc`

    Args:
        project_name: a `str`
        author_name: a `str`
        output_dir: a `str` as the output directory
    """
    output_path = os.path.join(output_dir, project_name, "doc")
    project_path = os.path.join(output_dir, project_name, project_name)

    apidoc.main(
        argv=[
            "",
            project_path,
            "-o",
            output_path,
            "-f",
            "-e",
            "-F",
            "-H",
            project_name,
            "-A",
            author_name,
            "-V",
            "0.1.0",
            "-R",
            "Initialize",
        ]
    )

    conf_file = os.path.join(output_path, "conf.py")
    with open(conf_file, "r") as conf:
        conf_content = conf.read()

    with open(conf_file, "w") as output:
        for key, value in CONF_RULES.items():
            conf_content = conf_content.replace(key, value)
        output.write(conf_content)
