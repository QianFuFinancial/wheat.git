# -*- coding: utf-8 -*-
# Author: heheqiao <614400597@qq.com>
CONF_RULES = {
    "like shown here.": "like shown here.\nimport sphinx_rtd_theme",
    "html_theme = 'alabaster'": (
        "html_theme = 'sphinx_rtd_theme'\nhtml_theme_path = "
        "[sphinx_rtd_theme.get_html_theme_path()]"
    ),
}

AVAILABLE_TYPES = ["cli", "api"]
