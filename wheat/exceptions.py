# -*- coding: utf-8 -*-
# Author: heheqiao <614400597@qq.com>


class ProjectGenerationError(Exception):
    """Exceptions raised when files not found in path"""

    def __init__(self, file_path):
        """Initialize

        Args:
            file_path: a `str`
        """
        self.file_path = file_path

    def __str__(self):
        return "Generation of {} failed".format(self.file_path)
